SRC=SM_CV.tex

.PHONY: all clean run

all:
	xelatex -interaction=nonstopmode --shell-escape $(SRC)

run: all

clean:
	rm -f *.aux *.bbl *.blg *.log *.out *.xml *.toc *.1 *.mp *.t1 $(subst .tex,,$(SRC))-blx.bib
